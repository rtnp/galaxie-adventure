extends Control

@onready var chatLog = $PanelContainer/VBoxContainer/RichTextLabel
@onready var inputLabel = $PanelContainer/VBoxContainer/HBoxContainer/Label
@onready var inputField = $PanelContainer/VBoxContainer/HBoxContainer/LineEdit

var groups = [
	{"name": "Team", "color": "#0aa8ff"},
	{"name": "Match", "color": "#ffb266"},
	{"name": "Global", "color": "#ffffff"},
	{"name": "Help", "color": "#7340ff"},
]

var group_index: int = 0
var user_name: String = "Tuxa"
var font_size: int = 6

func _ready() -> void:
	change_group(0)
	change_font_size(font_size)
	add_message("Godot", "The engine")
	inputField.visible = false
	inputLabel.visible = false

func _input(event: InputEvent) -> void:
	if event is InputEventKey:
		if event.pressed and event.keycode == KEY_ENTER:
			inputField.grab_focus()
			inputField.visible = true
			inputLabel.visible = true
		if event.pressed and event.keycode == KEY_ESCAPE:
			inputField.release_focus()
			inputField.visible = false
			inputLabel.visible = false
		if event.pressed and event.keycode == KEY_TAB:
			change_group(1)

func change_font_size(value: int = 6):
	chatLog.set("theme_override_font_sizes/bold_italics_font_size",value)
	chatLog.set("theme_override_font_sizes/italics_font_size",value)
	chatLog.set("theme_override_font_sizes/mono_font_size",value)
	chatLog.set("theme_override_font_sizes/normal_font_size",value)
	chatLog.set("theme_override_font_sizes/bold_font_size",value)
	inputField.set("theme_override_font_sizes/font_size",value)
	inputLabel.set("theme_override_font_sizes/font_size",value)
	
func change_group(value: int = 1):
	group_index += value
	if group_index > (groups.size() - 1):
		group_index = 0
	inputLabel.text = "[" + groups[group_index]["name"] + "] "
	inputLabel.set("theme_override_colors/font_color", Color(groups[group_index]["color"]))

# Returns escaped BBCode that won't be parsed by RichTextLabel as tags.
func escape_bbcode(bbcode_text) -> String:
	# We only need to replace opening brackets to prevent tags from being parsed.
	return bbcode_text.replace("[", "[lb]")

func add_message(username: String, message: String, group: int = 0) -> void:
	chatLog.append_text("[color=%s]%s: %s[/color]\n" % [
		groups[group]["color"],
		escape_bbcode(username),
		escape_bbcode(message)
		]
	)

func _on_line_edit_text_submitted(new_text: String) -> void:
	if new_text == "/h" or new_text == "/help":
		add_message("Help", "Not help yet", 3)
		inputField.text = ""
		return
	if new_text != "":
		add_message(user_name, new_text, group_index)
		inputField.text = ""
