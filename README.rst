Galaxie Adventure
*****************

Introduction
============
.

Réference
=========
La documentation se trouve dans le répertoire `docs/sources`

Developpement
=============

Image split
-----------

.. code:: shell

   convert ImageName.png -crop 32x32 %d.png

Player Mo
---------

.. code:: shell

	open https://sanderfrenken.github.io/Universal-LPC-Spritesheet-Character-Generator/#?body=Humanlike_white&shoes=Shoes_brown&legs=Pants_charcoal&vest=none&armour=none&clothes=Sleeveless_charcoal&jacket=none&hair=Buzzcut_platinum&sex=male&cape=none&facial=Facial_glasses&shadow=none&wound_arm=none&wound_brain=none&wound_ribs=none&bandana=none&apron=none&bandages=none&chainmail=none&jacket_collar=none&jacket_trim=none&belt=Leather_Belt_slate&eyes=Eyes_blue

Player
------

.. code:: shell

	open https://sanderfrenken.github.io/Universal-LPC-Spritesheet-Character-Generator/#?body=Humanlike_white&shoes=Shoes_brown&legs=Pantaloons_gray&vest=Vest_open_charcoal&armour=none&clothes=Sleeved_formal&jacket=Collared_coat_tan&hair=Mop_dark_gray
 



Générer la documentation
------------------------

.. code:: shell

	make docs

