Authors: Stephen Challener (Redshrike), Johannes Sjölund (wulax), Radomir Dopieralski, bluecarrot16, Thane Brimhall (pennomi), laetissima, Nila122

- body/male/human/white.png: by Stephen Challener (Redshrike), Johannes Sjölund (wulax). License(s): CC-BY-SA 3.0, GPL 3.0. 
    - https://opengameart.org/content/liberated-pixel-cup-lpc-base-assets-sprites-map-tiles
    - https://opengameart.org/content/lpc-medieval-fantasy-character-sprites

- hair/mop/male/dark_gray.png: by Radomir Dopieralski, bluecarrot16. License(s): CC-BY-SA 3.0, GPL 3.0. 
    - https://opengameart.org/content/alternate-lpc-character-sprites-george
    - https://opengameart.org/content/lpc-hair

- torso/clothes/longsleeve/male/formal.png: by bluecarrot16, Thane Brimhall (pennomi), laetissima, Johannes Sjölund (wulax). License(s): CC-BY-SA 3.0, GPL 3.0. 
    - https://opengameart.org/content/lpc-medieval-fantasy-character-sprites
    - https://opengameart.org/content/lpc-2-characters
    - https://opengameart.org/content/lpc-gentleman

- torso/jacket/collared/male/tan.png: by bluecarrot16. License(s): CC-BY-SA 3.0, GPL 3.0. 
    - https://opengameart.org/content/lpc-gentleman
    - https://opengameart.org/content/lpc-pirates

- torso/clothes/vest_open/male/charcoal.png: by bluecarrot16, Thane Brimhall (pennomi), laetissima. License(s): CC-BY-SA 3.0, GPL 3.0. 
    - https://opengameart.org/content/lpc-2-characters
    - https://opengameart.org/content/lpc-gentleman
    - https://opengameart.org/content/lpc-pirates

- legs/pantaloons/male/gray.png: by Nila122, Johannes Sjölund (wulax), Stephen Challener (Redshrike). License(s): CC-BY-SA 3.0, GPL 2.0, GPL 3.0. 
    - https://opengameart.org/content/lpc-pirates
    - https://opengameart.org/content/more-lpc-clothes-and-hair

- feet/shoes/male/brown.png: by Johannes Sjölund (wulax). License(s): CC-BY-SA 3.0, GPL 3.0. 
    - https://opengameart.org/content/lpc-medieval-fantasy-character-sprites