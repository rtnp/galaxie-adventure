extends CharacterBody2D

@onready var animationPlayer = $AnimationPlayer
@onready var animationTree = $AnimationTree
@onready var animationState = animationTree.get("parameters/playback")

@onready var label = $Label

const ACCELERATION = 575
const MAX_SPEED = 120
const FRICTION = 575

var _player_name: String = "Player"
@export var player_name: String:
	get:
		return _player_name
	set(value):
		if _player_name != value:
			_player_name = value
			label.text = self.player_name
			


var _hp: int = 2
var hp: int:
	get:
		return _hp
	set(value):
		if _hp != value:
			_hp = value
			if _hp <= 0:
				die()

func _physics_process(delta):
	var input_vector = Vector2.ZERO
	input_vector.x = Input.get_action_strength("ui_right") - Input.get_action_strength("ui_left")
	input_vector.y = Input.get_action_strength("ui_down") - Input.get_action_strength("ui_up")
	input_vector = input_vector.normalized()
	
	if input_vector != Vector2.ZERO:
		animationTree.set("parameters/Idle/blend_position", input_vector)
		animationTree.set("parameters/Walk/blend_position", input_vector)
		animationState.travel("Walk")
		velocity = velocity.move_toward(input_vector * MAX_SPEED, ACCELERATION * delta)
	else:
		animationState.travel("Idle")
		velocity = velocity.move_toward(Vector2.ZERO, FRICTION * delta)
		
	set_velocity(velocity)
	move_and_slide()
	velocity = velocity

func teleporte_to_location(new_location):
	position = new_location

func damage(dam: int) -> void:
	self.hp -= dam

func enable_hitbox() -> void:
	pass
	
func disable_hitbox() -> void:
	pass
	
func die() -> void:
	pass
